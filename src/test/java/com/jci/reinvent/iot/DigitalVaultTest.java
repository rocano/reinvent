package com.jci.reinvent.iot;

import com.amazonaws.services.lambda.runtime.LambdaLogger;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.Dsl;

public class DigitalVaultTest {

  public static void main(String[] args) throws Exception {
    try {
      LambdaLogger logger = System.out::println;
      AsyncHttpClient client = Dsl.asyncHttpClient();
      DigitalVaultUtils.Token token = DigitalVaultUtils.obtainOrRefresh(Optional.empty(), client, logger);
      CompletableFuture<DigitalVaultUtils.DeviceData> entityForDeviceFuture =
          DigitalVaultUtils.getEnrichmentDataForDevice("fb954c68-bf59-4a29-acfa-db9a0c5364b6", client, token, logger);
      DigitalVaultUtils.DeviceData data = entityForDeviceFuture.get();

      System.out.println(data);
    } finally {
      System.exit(0);
    }
  }
}
