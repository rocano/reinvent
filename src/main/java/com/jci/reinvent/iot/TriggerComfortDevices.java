package com.jci.reinvent.iot;

import com.amazonaws.services.iotdata.AWSIotData;
import com.amazonaws.services.iotdata.AWSIotDataClientBuilder;
import com.amazonaws.services.iotdata.model.PublishRequest;
import com.amazonaws.services.iotdata.model.PublishResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class TriggerComfortDevices implements RequestStreamHandler {

  ObjectMapper objectMapper = new ObjectMapper();
  AWSIotData awsIotClient = AWSIotDataClientBuilder.defaultClient();

  @Override
  public void handleRequest(InputStream input, OutputStream output, Context context) throws IOException {
    LambdaLogger logger = context.getLogger();
    JsonNode payload = objectMapper.readTree(input);
    logger.log("Received payload\n" + payload);
    int roomTemperature = payload.get("currentRoomTemperature").asInt();
    int outsideTemperature = payload.get("currentOutsideTemperature").asInt();
    int deviceSetpoint = payload.get("deviceSetpoint").asInt();
    int averageTemperature = payload.get("averageTemperature").asInt();
    int tempDelta = payload.get("deltaTemperatureRise").asInt();
    logger.log(String.format("Room temperature: %d, device setpoint: %d, 10min temperature delta: %d", roomTemperature, deviceSetpoint, tempDelta));

    List<ComfortDevice> comfortDevices =
        StreamSupport.stream(payload.get("climateControllers").spliterator(), false).map(node ->
            new ComfortDevice(node.get("thingName").asText(), node.get("deviceCommandTopic").asText())).collect(Collectors.toList());

    JsonNodeFactory jsonFactory = new JsonNodeFactory(false);
    ObjectNode desiredState =
        jsonFactory.objectNode().
        put("currentRoomTemperature", roomTemperature).
        put("currentOutsideTemperature", outsideTemperature).
        put("deviceSetpoint", deviceSetpoint).
        put("averageTemperature", averageTemperature).
        put("deltaTemperatureRise", tempDelta).
        put("comfortControllerActive", false);

    if (comfortDevices.isEmpty()) {
      logger.log("No climateControllers in the payload");
      return;
    }

    if (roomTemperature > deviceSetpoint) {
      if (tempDelta < 5) {
        logger.log("Turning on 1 comfort controller");

        JsonNode state = jsonFactory.objectNode().set("state", jsonFactory.objectNode().
            set("desired", desiredState.deepCopy().put("comfortControllerActive", true)));

        PublishResult res = publishIot(state, comfortDevices.get(0).deviceCommandTopic);
        logger.log(String.format("Result of publishing to %s: %s ", comfortDevices.get(0).deviceCommandTopic, res));

        state = jsonFactory.objectNode().set("state", jsonFactory.objectNode().set("desired", desiredState));
        for (int i = 1; i < comfortDevices.size(); i++) {
          ComfortDevice dev = comfortDevices.get(i);

          res = publishIot(state, dev.deviceCommandTopic);
          logger.log(String.format("Result of publishing to %s: %s ", dev.deviceCommandTopic, res));
        }

      } else {
        logger.log("Turning on all comfort controller");

        JsonNode state = jsonFactory.objectNode().set("state", jsonFactory.objectNode().
            set("desired", desiredState.deepCopy().put("comfortControllerActive", true)));

        for (ComfortDevice dev : comfortDevices) {
          PublishResult res = publishIot(state, dev.deviceCommandTopic);
          logger.log(String.format("Result of publishing to %s: %s ", dev.deviceCommandTopic, res));
        }
      }
    } else {
      JsonNode state = jsonFactory.objectNode().set("state", jsonFactory.objectNode().set("desired", desiredState));

      ByteBuffer bb = null;
      try {
        bb = ByteBuffer.wrap(objectMapper.writer().writeValueAsBytes(state));
      } catch (JsonProcessingException ex) {
        throw new IllegalStateException(ex);
      }
      for (ComfortDevice comfortDevice : comfortDevices) {
        PublishResult res = publishIot(bb, comfortDevice.deviceCommandTopic);
        logger.log(String.format("Result of publishing to %s: %s ", comfortDevice.deviceCommandTopic, res));
      }
    }
  }

  private PublishResult publishIot(JsonNode node, String topic) {
    ByteBuffer bb = null;
    try {
      bb = ByteBuffer.wrap(objectMapper.writer().writeValueAsBytes(node));
    } catch (JsonProcessingException ex) {
      throw new IllegalStateException(ex);
    }
    return publishIot(bb, topic);
  }

  private PublishResult publishIot(ByteBuffer payload, String topic) {
    PublishRequest publishRequest = new PublishRequest();
    publishRequest.setQos(0);
    publishRequest.setTopic(topic);
    publishRequest.setPayload(payload);
    return awsIotClient.publish(publishRequest);
  }

  private static class ComfortDevice {

    public final String thingName;
    public final String deviceCommandTopic;

    public ComfortDevice(String thingName, String deviceCommandTopic) {
      this.thingName = thingName;
      this.deviceCommandTopic = deviceCommandTopic;
    }

  }
}
