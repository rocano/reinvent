package com.jci.reinvent.iot;

import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.time.Instant;
import java.util.AbstractMap;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.BoundRequestBuilder;
import org.asynchttpclient.Response;
import org.asynchttpclient.util.Utf8UrlEncoder;

@SuppressWarnings({"ThrowableInstanceNotThrown", "ThrowableInstanceNeverThrown"})
public class DigitalVaultUtils {

  private static final String IAM_URL = "https://uat-api.digitalvault.cloud/idm/ims";
  private static final String ENTITY_SERVICE_URL = "https://uat-api.digitalvault.cloud/entity/api/v2.0";
  private static final String TIMESERIES_SERVICE_URL = "https://uat-api.digitalvault.cloud/timeseriesapi/v2.0";
  private static final String AERIS_WEATHER_URL = "https://uat-api.digitalvault.cloud/weather";
  private static final ObjectMapper objectMapper = new ObjectMapper();

  public static Token obtainOrRefresh(Optional<Token> token, AsyncHttpClient client, LambdaLogger logger) throws InterruptedException,
      ExecutionException,
      IOException {
    if (token.isPresent() && token.get().expiresAt.isAfter(Instant.now())) {
      return token.get();
    }
    logger.log("obtaining new IMS token");

    StringBuilder urlEncodedForm = new StringBuilder();
    urlEncodedForm.append("grant_type=password");
    Utf8UrlEncoder.encodeAndAppendFormElement(urlEncodedForm.append("&scope="), "read write securityapi_all weatherapi_all");
    Utf8UrlEncoder.encodeAndAppendFormElement(urlEncodedForm.append("&username="), System.getenv("iamUsername"));
    Utf8UrlEncoder.encodeAndAppendFormElement(urlEncodedForm.append("&password="), System.getenv("iamPassword"));

    BoundRequestBuilder request = client.preparePost(IAM_URL + "/connect/token").
        addHeader("Content-Type", "application/x-www-form-urlencoded").
        addHeader("Authorization", System.getenv("iamAuth")).
        setBody(urlEncodedForm.toString());

    long requestStartTime = System.currentTimeMillis();
    Response response = request.execute().get();
    long total = System.currentTimeMillis() - requestStartTime;
    logger.log(String.format("iam request time %d ms, status: %d", total, response.getStatusCode()));

    if (response.getStatusCode() == 200) {
      JsonNode json = objectMapper.readTree(response.getResponseBody());
      String tokenCode = Optional.ofNullable(json.get("access_token")).orElseThrow(() -> new IllegalStateException(
          "access_token not found in IAM payload")).asText();
      int expiresIn = Optional.ofNullable(json.get("expires_in")).orElseThrow(() ->
          new IllegalStateException("expires_in not found in IAM payload")).intValue();

      return new Token(tokenCode, Instant.now().plusSeconds(expiresIn - 5));
    } else {
      throw new IllegalStateException("Unexpected status code " + response.getStatusCode() + ": " + response.getStatusText() + "\n" +
          response.getResponseBody());
    }
  }

  public static CompletableFuture<DeviceData> getEnrichmentDataForDevice(String device, AsyncHttpClient asyncHttpClient, Token token, LambdaLogger logger) {
    BoundRequestBuilder request = asyncHttpClient.prepareGet(ENTITY_SERVICE_URL + "/room").
        addQueryParam("$expand", "device($expand=point)").
        addHeader("Authorization", "Bearer " + token.token);

    long start = System.currentTimeMillis();

    return request.execute().toCompletableFuture().thenApply((Response response) -> {
      long total = System.currentTimeMillis() - start;
      logger.log(String.format("entity system request time %d, status: %d", total, response.getStatusCode()));

      if (response.getStatusCode() == 200) {
        JsonNode payload;
        try {
          payload = objectMapper.readTree(response.getResponseBody());
        } catch (IOException ex) {
          throw new IllegalStateException(ex);
        }

        //find the room that contains this device, essentially
        //rooms.map(room -> (room, room.devices.find(_.id == device))).find(_._2.isDefined)
        Optional<AbstractMap.SimpleEntry<JsonNode, Optional<JsonNode>>> roomOpt =
            StreamSupport.stream(payload.spliterator(), false).map(room -> {
              JsonNode deviceNode = Optional.ofNullable(room.get("device")).orElseThrow(() ->
                  new IllegalStateException("field device not found in the room:\n" + room));

              return new AbstractMap.SimpleEntry<>(room,
                  StreamSupport.stream(deviceNode.spliterator(), false).filter(dev -> 
                      Optional.ofNullable(dev.get("id")).orElseThrow(() -> new IllegalStateException("field id not found in device from Entity System?\n" + dev)).
                          asText().equals(device)).findFirst());
            }).filter(entry -> entry.getValue().isPresent()).findFirst();

        if (!roomOpt.isPresent()) {
          throw new IllegalStateException("Could not find our device [" + device + "] in the entity service!");
        }

        JsonNode roomNode = roomOpt.get().getKey();
        JsonNode temperatureDeviceNode = roomOpt.get().getValue().get();

        int setpoint = 0;
        float latitude = 0;
        float longitude = 0;

        for (JsonNode point : temperatureDeviceNode.get("point")) {
          String pointName = Optional.ofNullable(point.get("name")).orElseThrow(() ->
              new IllegalStateException("Point doesn't have a field name.\n" + point)).asText();
          JsonNode valueNode = Optional.ofNullable(point.get("value")).orElseThrow(() ->
              new IllegalStateException("Point doesn't have a field value.\n" + point));

          if (pointName.equals("temperature setpoint")) setpoint = valueNode.intValue();
          if (pointName.equals("latitude")) latitude = valueNode.floatValue();
          if (pointName.equals("longitude")) longitude = valueNode.floatValue();
        }

        List<String> comfortDevices =
            StreamSupport.stream(roomNode.get("device").spliterator(), false).filter(deviceNode ->
                deviceNode.get("name").asText().startsWith("ComfortController")).map(n -> n.get("id").asText()).collect(Collectors.toList());

        return new DeviceData(setpoint, latitude, longitude, comfortDevices);
      } else {
        throw new IllegalStateException(String.format("Unexpected status response from Entity Service: %d: %s", response.getStatusCode(), response.getStatusText()));
      }

    });
  }

  public static CompletableFuture<TimeSeriesData> getTimeSeriesData(String device, AsyncHttpClient asyncHttpClient, Token token, LambdaLogger logger) {
    Instant tenMinAgo = Instant.now().minusSeconds(60 * 10);
    Instant elevenMinAgo = tenMinAgo.minusSeconds(60);
    BoundRequestBuilder rawSamplesReq = asyncHttpClient.prepareGet(TIMESERIES_SERVICE_URL + "/timeseries/" + device + "/samples").
        addQueryParam("startTime", elevenMinAgo.toString()).
        addQueryParam("endTime", tenMinAgo.toString()).
        addQueryParam("limit", "1").
        addHeader("Authorization", "Bearer " + token.token);

    BoundRequestBuilder average15minReq = asyncHttpClient.prepareGet(TIMESERIES_SERVICE_URL + "/timeseries/" + device + "/samples").
        addQueryParam("metric", "QuarterHourlyAverage").
        addQueryParam("limit", "1").
        addHeader("Authorization", "Bearer " + token.token);

    long start = System.currentTimeMillis();

    CompletableFuture<Optional<Integer>> temperatureDelta =
        rawSamplesReq.execute().toCompletableFuture().thenApply(response -> {

          long total = System.currentTimeMillis() - start;
          logger.log(String.format("average temp request time %d, status: %d", total, response.getStatusCode()));

          if (response.getStatusCode() == 200) {
            JsonNode payload;
            try {
              payload = objectMapper.readTree(response.getResponseBody());
            } catch (IOException ex) {
              throw new IllegalStateException(ex);
            }

            List<Integer> tempEntries = StreamSupport.stream(payload.spliterator(), false).
                map(n -> n.get("val").intValue()).
                collect(Collectors.toList());

            logger.log("Sample 10 minutes ago " + payload);

            if (tempEntries.isEmpty()) return Optional.empty();
            else return Optional.of(tempEntries.get(0));
          } else {
            throw new IllegalStateException(String.format("Unexpected status response from TimeSeries Service: %d: %s", response.getStatusCode(), response.getStatusText()));
          }
        });

    CompletableFuture<Optional<Integer>> averageTempFuture =
        average15minReq.execute().toCompletableFuture().thenApply(response -> {

          long total = System.currentTimeMillis() - start;
          logger.log(String.format("QuarterHourlyAverage request time %d, status: %d", total, response.getStatusCode()));

          if (response.getStatusCode() == 200) {
            JsonNode payload;
            try {
              payload = objectMapper.readTree(response.getResponseBody());
            } catch (IOException ex) {
              throw new IllegalStateException(ex);
            }

            Iterator<JsonNode> elements = payload.iterator();
            Optional<Integer> tempAverage = elements.hasNext() ? Optional.of(elements.next().get("val").floatValue()).map(f -> f.intValue()) : Optional.empty();

            return tempAverage;

          } else {
            throw new IllegalStateException(String.format("Unexpected status response from TimeSeries Service: %d: %s", response.getStatusCode(), response.getStatusText()));
          }
        });

    return averageTempFuture.thenCombine(temperatureDelta, (avgTemp, temp10MinAgo) ->
        new TimeSeriesData(avgTemp, temp10MinAgo));
  }

  public static CompletableFuture<Integer> getOutsideTemperature(String zipcode, AsyncHttpClient asyncHttpClient, Token token, LambdaLogger logger) {
    BoundRequestBuilder request = asyncHttpClient.prepareGet(AERIS_WEATHER_URL + "/AerisWeather/observations/archive/" + zipcode).
        addHeader("Authorization", "Bearer " + token.token);

    long start = System.currentTimeMillis();
    return request.execute().toCompletableFuture().thenApply((Response response) -> {
      long total = System.currentTimeMillis() - start;
      logger.log(String.format("weather request time %d, status: %d", total, response.getStatusCode()));

      if (response.getStatusCode() == 200) {
        JsonNode payload;
        try {
          payload = objectMapper.readTree(response.getResponseBody());
        } catch (IOException ex) {
          throw new IllegalStateException(ex);
        }

        if (!payload.get("success").asBoolean()) {
          throw new IllegalStateException("Weather response was not successful:\n" + payload);
        }

        JsonNode periods = payload.at("/response/periods");
        if (periods.isMissingNode()) {
          throw new IllegalStateException("/response/periods field not found:\n" + payload);
        }

        Function<JsonNode, Supplier<? extends RuntimeException>> noTimestamp = n -> () -> new IllegalStateException("no timestamp field inside ob?\n" + n);
        Optional<JsonNode> latestTemp = StreamSupport.stream(periods.spliterator(), false).
            map(n -> Optional.ofNullable(n.get("ob")).orElseThrow(() -> new IllegalStateException("No ob field inside period?\n" + n))).
            sorted((n1, n2) ->
                (int) (Optional.ofNullable(n2.get("timestamp")).orElseThrow(noTimestamp.apply(n2)).longValue() -
                Optional.ofNullable(n1.get("timestamp")).orElseThrow(noTimestamp.apply(n1)).longValue())).findFirst();

        if (!latestTemp.isPresent()) {
          throw new IllegalStateException("no temperature reading found?:\n" + payload);
        }

        return latestTemp.map(n ->
            Optional.ofNullable(n.get("tempF")).orElseThrow(() -> new IllegalStateException("No tempF field in ob?\n" + n)).intValue()).get();

      } else {
        throw new IllegalStateException(String.format("Unexpected status response from Entity Service: %d: %s", response.getStatusCode(), response.getStatusText()));
      }

    });

  }

  public static class Token {

    public final String token;
    public final Instant expiresAt;

    public Token(String token, Instant expiresAt) {
      this.token = token;
      this.expiresAt = expiresAt;
    }
  }

  public static class DeviceData {

    public final int deviceSetpoint;
    public final float latitude;
    public final float longitude;
    public final List<String> comfortControllers;

    public DeviceData(int deviceSetpoint, float latitude, float longitude, List<String> comfortControllers) {
      this.deviceSetpoint = deviceSetpoint;
      this.latitude = latitude;
      this.longitude = longitude;
      this.comfortControllers = comfortControllers;
    }

    @Override
    public String toString() {
      return "DeviceData{" + "deviceSetpoint=" + deviceSetpoint + ", latitude=" + latitude + ", longitude=" + longitude +
          ", comfortControllers=" + comfortControllers + '}';
    }
    
  }

  public static class TimeSeriesData {

    public final Optional<Integer> averageTemperature;
    public final Optional<Integer> temperature10MinAgo;

    public TimeSeriesData(Optional<Integer> averageTemperature, Optional<Integer> temperature10MinAgo) {
      this.averageTemperature = averageTemperature;
      this.temperature10MinAgo = temperature10MinAgo;
    }

  }
}
