package com.jci.reinvent.iot;

import com.amazonaws.services.iotdata.AWSIotData;
import com.amazonaws.services.iotdata.AWSIotDataClientBuilder;
import com.amazonaws.services.iotdata.model.PublishRequest;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jci.reinvent.iot.DigitalVaultUtils.DeviceData;
import com.jci.reinvent.iot.DigitalVaultUtils.TimeSeriesData;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.Dsl;

@SuppressWarnings({"ThrowableInstanceNotThrown", "ThrowableInstanceNeverThrown"})
public class EnrichData implements RequestStreamHandler {

  private static final String ENTITY_SERVICE_URL = "https://uat-api.digitalvault.cloud/entity/api/v2.0";
  private static final String TIMESERIES_SERVICE_URL = "https://uat-api.digitalvault.cloud/timeseriesapi/v2.0";
  private static final String AERIS_WEATHER_URL = "https://uat-api.digitalvault.cloud/weather";

  ObjectMapper objectMapper = new ObjectMapper();
  AsyncHttpClient asyncHttpClient = Dsl.asyncHttpClient();
  AWSIotData awsIotClient = AWSIotDataClientBuilder.defaultClient();
  DigitalVaultUtils.Token token = null;

  @Override
  public void handleRequest(InputStream input, OutputStream output, Context context) throws IOException {
    JsonNode tree = objectMapper.readTree(input);
    LambdaLogger logger = context.getLogger();
    logger.log("Received payload\n" + tree);

    Optional<UUID> thingNameOpt = IotUtils.extractThingName(tree.get("topic").asText());
    Optional<JsonNode> state = Optional.ofNullable(tree.get("state"));
    if (!thingNameOpt.isPresent()) {
      logger.log("ERROR: could not find thing name in topic field?");
      return;
    }
    if (!state.isPresent()) {
      logger.log("ERROR: could not find data field in the payload?");
      return;
    }
    UUID thingName = thingNameOpt.get();
    JsonNode data = state.get();
    JsonNode temperature = data.at("/reported/Temperature");
    if (temperature.isMissingNode()) throw new IllegalStateException("did not find temperature field in payload");

    enrich(thingName.toString(), temperature.intValue(), logger, context);
//    mockEnrich(logger, context, thingName, data);
  }

  private void enrich(String thingName, int currentTemperature, LambdaLogger logger, Context context) throws IOException {
    try {
      token = DigitalVaultUtils.obtainOrRefresh(Optional.ofNullable(token), asyncHttpClient, logger);
    } catch (InterruptedException | ExecutionException ex) {
      logger.log("ERROR: Failed obtaining token! " + ex);
      return;
    }
    //get Entity Services data
    CompletableFuture<DeviceData> entityDataFuture = DigitalVaultUtils.getEnrichmentDataForDevice(thingName, asyncHttpClient, token, logger);
    //get Time Series data for the counters
    CompletableFuture<TimeSeriesData> timeSeriesCountersFuture = DigitalVaultUtils.getTimeSeriesData(thingName, asyncHttpClient, token, logger);
    //get weather data
    CompletableFuture<Integer> weatherDataFuture = DigitalVaultUtils.getOutsideTemperature(System.getenv("zipcode"), asyncHttpClient, token, logger);
    try {
      CompletableFuture.allOf(entityDataFuture, timeSeriesCountersFuture, weatherDataFuture).get(50, TimeUnit.SECONDS);
    } catch (InterruptedException | ExecutionException | TimeoutException ex) {
      throw new IllegalStateException(ex);
    }
    //here we have a guarantee that all of the futures completed
    DeviceData entityData = entityDataFuture.getNow(null);
    TimeSeriesData timeSeriesCounters = timeSeriesCountersFuture.getNow(null);
    Integer weatherData = weatherDataFuture.getNow(null);

    JsonNodeFactory nodeFactory = objectMapper.getNodeFactory();
    ObjectNode resultDocument = nodeFactory.objectNode().
        put("currentRoomTemperature", currentTemperature).
        put("currentOutsideTemperature", weatherData).
        put("deviceUUID", thingName).
        put("deviceSetpoint", entityData.deviceSetpoint).
        put("averageTemperature", timeSeriesCounters.averageTemperature.orElse(currentTemperature)).
        put("deltaTemperatureRise", timeSeriesCounters.temperature10MinAgo.map(t -> currentTemperature - t).orElse(0));

    resultDocument.set("deviceLocation", nodeFactory.objectNode().put("type", "Point").
        set("coordinates", nodeFactory.arrayNode().add(entityData.latitude).add(entityData.longitude)));

    //TODO: enrich data and publish
    ArrayNode climateControllersJArray = resultDocument.putArray("climateControllers");
    for (String controller : entityData.comfortControllers) {
      climateControllersJArray.add(nodeFactory.objectNode().put("thingName", controller).
          put("deviceCommandTopic", "$aws/things/" + controller + "/shadow/update"));
    }


    ByteBuffer payload = null;
    try {
      payload = ByteBuffer.wrap(objectMapper.writer().writeValueAsBytes(resultDocument));
    } catch (JsonProcessingException ex) {
      throw new IllegalStateException(ex);
    }

    PublishRequest publishRequest = new PublishRequest();
    publishRequest.setQos(0);
    publishRequest.setTopic(System.getenv("actionTopic"));
    publishRequest.setPayload(payload);
    awsIotClient.publish(publishRequest);
  }

  private void mockEnrich(LambdaLogger logger, Context context, UUID thingName, JsonNode state) throws IOException {

    String climateControllersSetting = System.getenv("climateControllers");
    if (climateControllersSetting == null) throw new IllegalStateException("Environment setting climateControllers is not set");

    String[] controllers = climateControllersSetting.split(",");

    JsonNodeFactory jsonFactory = new JsonNodeFactory(false);
    ObjectNode template = (ObjectNode) objectMapper.readTree(getClass().getResourceAsStream("/enrich-mock.json"));
    ArrayNode climateControllersJArray = template.putArray("climateControllers");
    for (String controller : controllers) {
      climateControllersJArray.add(jsonFactory.objectNode().put("thingName", controller).
          put("deviceCommandTopic", "$aws/things/" + controller + "/shadow/update"));
    }

    ByteBuffer payload = null;
    try {
      payload = ByteBuffer.wrap(objectMapper.writer().writeValueAsBytes(template));
    } catch (JsonProcessingException ex) {
      throw new IllegalStateException(ex);
    }

    PublishRequest publishRequest = new PublishRequest();
    publishRequest.setQos(0);
    publishRequest.setTopic(System.getenv("actionTopic"));
    publishRequest.setPayload(payload);
    awsIotClient.publish(publishRequest);
  }
}
