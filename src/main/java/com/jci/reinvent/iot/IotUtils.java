package com.jci.reinvent.iot;

import java.util.Optional;
import java.util.UUID;

/**
 *
 * @author rcano
 */
public class IotUtils {
  public static Optional<UUID> extractThingName(String arn) {
    String[] parts = arn.split("/");
    if (parts.length < 2) return Optional.empty();
    try {
      return Optional.of(UUID.fromString(parts[2]));
    } catch (IllegalArgumentException e) {
      return Optional.empty();
    }
  }
}
