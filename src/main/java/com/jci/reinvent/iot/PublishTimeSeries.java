package com.jci.reinvent.iot;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.BoundRequestBuilder;
import org.asynchttpclient.Dsl;
import org.asynchttpclient.Response;

public class PublishTimeSeries implements RequestStreamHandler {

  private static final String TIMESERIES_URL = "https://uat-api.digitalvault.cloud/timeseriesapi/v2.0";

  AsyncHttpClient asyncHttpClient = Dsl.asyncHttpClient();
  DigitalVaultUtils.Token token = null;
  ObjectMapper objectMapper = new ObjectMapper();

  @Override
  public void handleRequest(InputStream input, OutputStream output, Context context) throws IOException {
    ObjectMapper jsonReader = new ObjectMapper();
    JsonNode tree = jsonReader.readTree(input);
    LambdaLogger logger = context.getLogger();
    logger.log("Received payload\n" + tree);

    try {
      token = DigitalVaultUtils.obtainOrRefresh(Optional.ofNullable(token), asyncHttpClient, logger);
    } catch (InterruptedException | ExecutionException ex) {
      logger.log("ERROR: Failed obtaining token! " + ex);
      return;
    }

    UUID thingName = IotUtils.extractThingName(tree.get("topic").textValue()).
            orElseThrow(() -> new IllegalStateException("thing name not found in payload"));

    int temperature = Optional.ofNullable(tree.at("/state/reported").get("Temperature")).orElseThrow(() -> new IllegalStateException(
            "/state/reported/Temperature field not found in payload")).intValue();

    ArrayNode timeSeriesPayload = objectMapper.getNodeFactory().arrayNode();
    timeSeriesPayload.addObject().
            put("timeseriesId", thingName.toString()).
            put("val", temperature).
            put("timestamp", Instant.now().toString()).
            put("metric", "Raw");


    BoundRequestBuilder request =
            asyncHttpClient.preparePost(TIMESERIES_URL + "/timeseries/" + thingName + "/samples").
                    addHeader("Content-Type", "application/json").
                    addHeader("Authorization", "Bearer " + token.token).
                    setBody(objectMapper.writeValueAsString(timeSeriesPayload));

    long reqStart = System.currentTimeMillis();
    Response response;
    try {
      response = request.execute().get();
    } catch (InterruptedException | ExecutionException ex) {
      logger.log("ERROR: Failed publishing data to TimeSeries " + ex);
      return;
    }
    long totalTime = System.currentTimeMillis() - reqStart;
    logger.log(String.format("publish to time series time %d ms, status: %d", totalTime, response.getStatusCode()));
  }

}
